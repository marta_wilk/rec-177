package com.example.contactsbook;

import java.sql.SQLException;
import java.util.List;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.data.Container.Filterable;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.filter.SimpleStringFilter;
import com.vaadin.data.util.sqlcontainer.SQLContainer;
import com.vaadin.data.util.sqlcontainer.connection.SimpleJDBCConnectionPool;
import com.vaadin.data.util.sqlcontainer.query.TableQuery;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
@Theme("contactsbook")
public class ContactsbookUI extends UI  {

	@WebServlet(value = "/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = ContactsbookUI.class)
	public static class Servlet extends VaadinServlet {
	}


	
	Baza b = new Baza();
	private Panel wybranyKontakt = new Panel("Contact");
	
	
	
	private Window oknoDodajKontakt = new Window("Add Contact:");
	private TextField firstName = new TextField("First Name:");
	private TextField lastName = new TextField("Last Name:");
	private TextField phone = new TextField("Phone:");
	private TextField mail = new TextField("Mail:");
	private TextField address = new TextField("Address:");
	private TextField city = new TextField("City:");
	private TextField zip = new TextField("Zip:");
	private CheckBox isFriend = new CheckBox("isFriend", false);

	private VerticalLayout addContactLayyout = new VerticalLayout();
	private Button closeAddContact = new Button("cancel");
	private Button okAddContact = new Button("add");
	private VerticalLayout zawartoscOknaDodajKontakt = new VerticalLayout();

	ComboBox kontakty = new ComboBox("Lista kontakt�w", b.selectAllFromContacts());
	
	@Override
	protected void init(VaadinRequest request) {
		final VerticalLayout layout = new VerticalLayout();
		layout.setMargin(true);
		setContent(layout);

		Button pokaz = new Button ("show more");
		Button dodaj = new Button("add contact");
		Button usun = new Button("delete contact");
		final Button zapisz = new Button("save");
		
		pokaz.addClickListener(new Button.ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
			
				if(kontakty.getValue()!=null){
					
					Contact c = null;
					c= (Contact) kontakty.getValue();
					layout.addComponent(wybranyKontakt);
					VerticalLayout selectedContact = new VerticalLayout();
					selectedContact.removeAllComponents();
					
					selectedContact.removeAllComponents();
					
					usunWpisaneDane();
					
					
					selectedContact.addComponent(firstName);
					selectedContact.addComponent(lastName);
					selectedContact.addComponent(phone);
					selectedContact.addComponent(mail);
					selectedContact.addComponent(address);
					selectedContact.addComponent(city);
					selectedContact.addComponent(zip);
					selectedContact.addComponent(isFriend);
					firstName.setValue(c.getFirstName());
					lastName.setValue(c.getLastName()); 
					phone.setValue(c.getPhone());
					mail.setValue(c.getEmail());
					address.setValue(c.getAddress()); 
					city.setValue(c.getCity()); 
					zip.setValue(c.getZip());
					isFriend.setValue(c.isFriend());
					
					selectedContact.addComponent(zapisz);
					wybranyKontakt.setContent(selectedContact);
				}
				
				
			}
		});

		zapisz.addClickListener(new Button.ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {

				Baza b2= new Baza();
				Contact c = null;
				c= (Contact) kontakty.getValue();
				b2.updateContact(c.getContact_ID(),firstName.getValue(),
				lastName.getValue(),
				phone.getValue(),
				mail.getValue(),
				address.getValue(), 
				city.getValue(),
				zip.getValue(),
				isFriend.getValue());
				
				kontakty.removeAllItems();
				kontakty = new ComboBox("Lista kontakt�w", b2.selectAllFromContacts());
				b2.closeConnection();
			}
		});
		
		dodaj.addClickListener(new Button.ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				zawartoscOknaDodajKontakt.removeAllComponents();
				usunWpisaneDane();
				zawartoscOknaDodajKontakt.addComponent(firstName);
				zawartoscOknaDodajKontakt.addComponent(lastName);
				zawartoscOknaDodajKontakt.addComponent(phone);
				zawartoscOknaDodajKontakt.addComponent(mail);
				zawartoscOknaDodajKontakt.addComponent(address);
				zawartoscOknaDodajKontakt.addComponent(city);
				zawartoscOknaDodajKontakt.addComponent(zip);
				zawartoscOknaDodajKontakt.addComponent(isFriend);
				HorizontalLayout poziomo = new HorizontalLayout();

				poziomo.addComponent(okAddContact);
				poziomo.addComponent(closeAddContact);
				poziomo.setSpacing(true);
				zawartoscOknaDodajKontakt.addComponent(poziomo);
				
				zawartoscOknaDodajKontakt.setSpacing(true);
				zawartoscOknaDodajKontakt.setMargin(true);

				oknoDodajKontakt.setModal(true);
				oknoDodajKontakt.setContent(zawartoscOknaDodajKontakt);

				UI.getCurrent().removeWindow(oknoDodajKontakt);
				UI.getCurrent().addWindow(oknoDodajKontakt);
				
			}
		});
		
		closeAddContact.addClickListener(new Button.ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				UI.getCurrent().removeWindow(oknoDodajKontakt);
				usunWpisaneDane();
			}
		});
		okAddContact.addClickListener(new Button.ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				
				Baza b1= new Baza();
				b1.insertContact(firstName.getValue(), lastName.getValue(), phone.getValue(), mail.getValue(), address.getValue(), city.getValue(), zip.getValue(), isFriend.getValue());
				UI.getCurrent().removeWindow(oknoDodajKontakt);
				kontakty.removeAllItems();
				kontakty = new ComboBox("Lista kontakt�w", b1.selectAllFromContacts());
				b1.closeConnection();
			}
		});
		
		usun.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {

				Contact c = null;
				c= (Contact) kontakty.getValue();
				Baza b3 = new Baza();
				b3.deleteContact(c.getContact_ID());
				kontakty.removeAllItems();
				kontakty = new ComboBox("Lista kontakt�w", b3.selectAllFromContacts());
				
				b3.closeConnection();
				
			}
		});
		layout.addComponent(kontakty);
		layout.addComponent(pokaz);
		layout.addComponent(dodaj);
		layout.addComponent(usun);
		
		

		
	}
	public void usunWpisaneDane(){
		

		firstName.setValue("");
		lastName.setValue(""); 
		phone.setValue("");
		mail.setValue("");
		address.setValue(""); 
		city.setValue(""); 
		zip.setValue("");
		isFriend.setValue(false);
	}
}