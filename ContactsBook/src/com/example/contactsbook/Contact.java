package com.example.contactsbook;

public class Contact {
	
	
	/**
	 * @param contact_ID
	 * @param firstName
	 * @param lastName
	 * @param phone
	 * @param email
	 * @param address
	 * @param city
	 * @param zip
	 * @param isFriend
	 */
	public Contact(int contact_ID, String firstName, String lastName,
			String phone, String email, String address, String city, String zip,
			boolean isFriend) {
		super();
		Contact_ID = contact_ID;
		this.firstName = firstName;
		this.lastName = lastName;
		this.phone = phone;
		this.email = email;
		this.address = address;
		this.city = city;
		this.zip = zip;
		this.isFriend = isFriend;
	}

	private int Contact_ID;
	private String firstName;
	private String lastName;
	private String phone;
	private String email;
	private String address;
	private String city;
	private String zip;
	private boolean isFriend;
	

	public int getContact_ID() {
		return Contact_ID;
	}

	public void setContact_ID(int contact_ID) {
		Contact_ID = contact_ID;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public boolean isFriend() {
		return isFriend;
	}

	public void setFriend(boolean isFriend) {
		this.isFriend = isFriend;
	}

	@Override
	public String toString() {
		return firstName+" " +lastName;
	}

}
