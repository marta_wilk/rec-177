package com.example.contactsbook;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.sql.Array;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


public class Baza {

	private Connection connection = null;
	private Statement st = null;
	private ResultSet result = null;
	private PreparedStatement pst = null;

	public Baza()  {
		try {
			Class.forName("org.postgresql.Driver");

		} catch (ClassNotFoundException e) {

			System.out.println("can not found! plz check the jar file");
			e.printStackTrace();
			return;

		}

		System.out.println("PostgreSQL JDBC Driver connected!");

		try {
			connection = DriverManager.getConnection(
					"jdbc:postgresql://localhost:5432/contact", "postgres",
					"baza");

			if (connection != null) {
				System.out.println("you connected now!");
			}

			st = connection.createStatement();

		} catch (SQLException e) {
			System.err.println("Problem z otwarciem polaczenia");
			e.printStackTrace();
		}
		 
		//st.executeUpdate("Drop TABLE  CONTACTS ");
		//st.executeUpdate("CREATE TABLE  CONTACTS (contact_id serial PRIMARY KEY, firstName  CHAR(30), lastName CHAR(30), phone CHAR(12), email CHAR(40), address CHAR(50), city CHAR(20),zip CHAR(6), isFriend Boolean);");

	}

	public static void main(String[] argv) throws SQLException  {

		Baza b = new Baza();

		//b.insertContact("test", "test2", "123456789", "1234@gmail.com", "le�na", "Warszawa", "00-001", true);
		b.selectAllFromContacts();
		b.closeConnection();
	}

	
	
	public void closeConnection() {
		try {
			connection.close();
			System.out.println("Poprawnie roz��czono z baz� danych");
		} catch (SQLException e) {
			System.err.println("Problem z zamknieciem polaczenia");
			e.printStackTrace();
		}
	}

	public void insertContact(String firstName,String lastName,String phone,String email,String address,
			String city,String zip, boolean isFriend) {

		try {

			String stm = "INSERT INTO CONTACTS (firstName, lastName, phone, email, address,	city, zip, isFriend) VALUES (?,?,?,?,?,?,?,?)";

			pst = connection.prepareStatement(stm);
			pst.setString(1, firstName);
			pst.setString(2, lastName);
			pst.setString(3, phone);
			pst.setString(4, email);
			pst.setString(5, address);
			pst.setString(6, city);
			pst.setString(7, zip);
			pst.setBoolean(8, isFriend);
			pst.executeUpdate();
			System.out.println(pst);

		} catch (SQLException e) {
			System.out.println("Problem! Nie dodano Kontaktu");
			e.printStackTrace();
		}

	}

	public void deleteContact(int id) {

		try {
			pst = connection
					.prepareStatement("DELETE FROM CONTACTS WHERE contact_id = ?;");
			pst.setInt(1, id);
			pst.executeUpdate();

		} catch (SQLException e) {
			System.out.println("b��d Nie usun��em Kontaktu");
			e.printStackTrace();
		}

	}
	

	public void updateContact(int ID,String firstName , String lastName,String phone,String email,String address,String city,String zip, Boolean isFriend){
		
		try {
			pst = connection
					.prepareStatement("UPDATE CONTACTS SET firstName = ? WHERE contact_id = ?;");
			pst.setString(1, firstName);
			pst.setInt(2, ID);
			pst.executeUpdate();
			
			pst = connection
					.prepareStatement("UPDATE CONTACTS SET lastName = ? WHERE contact_id = ?;");
			pst.setString(1, lastName);
			pst.setInt(2, ID);
			pst.executeUpdate();
			
			pst = connection
					.prepareStatement("UPDATE CONTACTS SET phone = ? WHERE contact_id = ?;");
			pst.setString(1, phone);
			pst.setInt(2, ID);
			pst.executeUpdate();
			
			pst = connection
					.prepareStatement("UPDATE CONTACTS SET email = ? WHERE contact_id = ?;");
			pst.setString(1, email);
			pst.setInt(2, ID);
			pst.executeUpdate();
			
			pst = connection
					.prepareStatement("UPDATE CONTACTS SET address = ? WHERE contact_id = ?;");
			pst.setString(1, address);
			pst.setInt(2, ID);
			pst.executeUpdate();
			
			pst = connection
					.prepareStatement("UPDATE CONTACTS SET city = ? WHERE contact_id = ?;");
			pst.setString(1, city);
			pst.setInt(2, ID);
			pst.executeUpdate();
			
			pst = connection
					.prepareStatement("UPDATE CONTACTS SET zip = ? WHERE contact_id = ?;");
			pst.setString(1, zip);
			pst.setInt(2, ID);
			pst.executeUpdate();
			
			pst = connection
					.prepareStatement("UPDATE CONTACTS SET isFriend = ? WHERE contact_id = ?;");
			pst.setBoolean(1, isFriend);
			pst.setInt(2, ID);
			pst.executeUpdate();
			


		} catch (SQLException e) {
			System.out.println("b��d zmiany danych");
			e.printStackTrace();
		}
	}

	public List<Contact> selectAllFromContacts() {
		LinkedList allContacts = new LinkedList<Contact>();
		try {
			pst = connection.prepareStatement("SELECT * FROM CONTACTS");

			result = pst.executeQuery();
			while (result.next()) {
				System.out.print(result.getInt(1)); // id
				System.out.print(": ");
				System.out.print(result.getString(2)); // nazwa
				System.out.println("");
				
				allContacts.add(new Contact(result.getInt(1), result.getString(2), result.getString(3), result.getString(4), result.getString(5),
						result.getString(6), result.getString(7), result.getString(8), result.getBoolean(9)));

			}

		} catch (SQLException e) {
			System.out.println("nie da�o rady wczyta� danych??");
			e.printStackTrace();
		}
		return allContacts;
	}



}
